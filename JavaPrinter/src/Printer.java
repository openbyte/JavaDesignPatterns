import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;

public class Printer {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Printer printer = new Printer();
		try {
			PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
			System.out.println("Number of print services: " + printServices.length);

			for (PrintService printer1 : printServices)
				System.out.println("Printer: " + printer1.getName());
			// boolean b = printer.print("192.168.10.253", 9100, "hello",
			// "GB-2312", 5);
			// System.out.println("b-->" + b);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Printer_() throws IOException {
		Socket client = new java.net.Socket();
		PrintWriter socketWriter;
		client.connect(new InetSocketAddress("192.168.10.253", 9100), 1000); // 创建一个
		socketWriter = new PrintWriter(client.getOutputStream());// 创建输入输出数据流
		socketWriter.println("巧富餐饮软件后厨单据");
		socketWriter.println("桌位 14 桌，人数 3 ");
		socketWriter.println("跺脚鱼头 1 份");
		socketWriter.write(0x1c);
	}

	void getList() {
		PrintService[] service = PrintServiceLookup.lookupPrintServices(null, null);

		for (int i = 0; i < service.length; i++) {
			System.out.println(service[i].getName());
		}
	}

	private boolean print(String ip, int port, String str, String code, int skip) throws Exception {
		Socket client = new java.net.Socket();
		PrintWriter socketWriter;
		client.connect(new InetSocketAddress(ip, port), 1000); // 创建一个 socket
		socketWriter = new PrintWriter(client.getOutputStream());// 创建输入输出数据流
		socketWriter.println("巧富餐饮软件后厨单据");
		socketWriter.println("桌位 14 桌，人数 3 ");
		socketWriter.println("跺脚鱼头 1 份");
		/* 纵向放大一倍 */
		socketWriter.write(0x1c);
		socketWriter.write(0x21);
		socketWriter.write(8);
		socketWriter.write(0x1b);
		socketWriter.write(0x21);
		socketWriter.write(8);
		socketWriter.println(str);
		// 打印条形码
		socketWriter.write(0x1d);
		socketWriter.write(0x68);
		socketWriter.write(120);
		socketWriter.write(0x1d);
		socketWriter.write(0x48);
		socketWriter.write(0x01);
		socketWriter.write(0x1d);
		socketWriter.write(0x6B);
		socketWriter.write(0x02);
		socketWriter.println(code);
		socketWriter.write(0x00);
		System.out.println("5555-->>>" + socketWriter.checkError());
		for (int i = 0; i < skip; i++) {
			socketWriter.println(" ");// 打印完毕自动走纸
		}
		return true;
	}
}
