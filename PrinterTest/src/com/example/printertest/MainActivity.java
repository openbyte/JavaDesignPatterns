package com.example.printertest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	Map<String, String> mapgs = new HashMap<String, String>();
	List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
	Button button;
	String sn = "201303280005";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		String str = "我对面的风格吧";
		try {
			String str1 = "";
			for (byte b : str.getBytes("GBK")) {
				str1 += b + ",";
			}
			System.out.println("str1-->>" + str1);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mapgs.put("GS_Name", " 武汉联兴科技有限公司");
		mapgs.put("GS_Address", "武汉市解放大道2679号");
		mapgs.put("GS_Tel", "13507115045");
		mapgs.put("GS_Qq", "138027869");
		Map<String, Object> map1 = new HashMap<String, Object>();
		map1.put("cai_name", "三鲜汤");
		map1.put("cai_price", "38");
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("cai_name", "煤烧猎手");
		map2.put("cai_price", "68");
		Map<String, Object> map3 = new HashMap<String, Object>();
		map3.put("cai_name", "海味三珍");
		map3.put("cai_price", "188");
		lists.add(map1);
		lists.add(map2);
		lists.add(map3);
		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * SocketClient client = new SocketClient("192.168.1.254",9100);
				 * client.sendMsg(lists,mapgs,sn);
				 */
				try {
					new PrintLine().print(lists, mapgs, sn);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});

	}

}
