package com.hero.org;

import java.util.ArrayList;

public class ConcretePrototype extends Prototype {

	public void show() {
		System.out.println("原型模式实现类");
	}

	@Override
	public String toString() {
		return super.toString() + "\n models=" + models + "]";
	}

	public void setModels(ArrayList<Model> models) {
		this.models = models;
	}

	public ArrayList<Model> getModels() {
		return models;
	}
}
