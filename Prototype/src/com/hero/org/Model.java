package com.hero.org;

public class Model implements Cloneable {
	private String name;
	private String address;
	private String Phone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	@Override
	public String toString() {
		return "\nModel [name=" + name + ", address=" + address + ", Phone=" + Phone + "]";
	}

	@Override
	public Model clone() {
		Model prototype = null;
		try {
			prototype = (Model) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return prototype;
	}

}
