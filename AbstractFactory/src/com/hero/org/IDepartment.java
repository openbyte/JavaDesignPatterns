package com.hero.org;

public interface IDepartment {
	void Insert(Department info);
	Department getDepartment(int id);
}
