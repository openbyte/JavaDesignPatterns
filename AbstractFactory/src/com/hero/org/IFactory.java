package com.hero.org;

public interface IFactory {
	IUser createUser();

	IDepartment createDepartment();
}
