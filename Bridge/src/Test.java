/**
 * A test client
 */
public class Test {
	public Test() {
	}

	public static void main(String[] args) {

		Text myText = new TextBold(TextImpMac.class);
		myText.DrawText("=== A test String ===");
		myText = new TextBold(TextImpLinux.class);
		myText.DrawText("=== A test String ===");
		System.out.println("------------------------------------------");
		myText = new TextItalic(TextImpMac.class);
		myText.DrawText("=== A test String ===");
		myText = new TextItalic(TextImpLinux.class);
		myText.DrawText("=== A test String ===");
	}
}