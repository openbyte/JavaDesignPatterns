package com.hero.org;

public class ConcreteStateC extends State {
	@Override
	public void Handle(Context context) {
		System.out.println(" 这是状态 StateC");
		context.setState(new ConcreteStateD());
	}
}
