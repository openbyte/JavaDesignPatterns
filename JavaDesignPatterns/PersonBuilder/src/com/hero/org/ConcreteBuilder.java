package com.hero.org;

public class ConcreteBuilder extends PersonBuilder {
	Person person;

	public ConcreteBuilder() {
		person = new Person();
	}

	@Override
	public Person getPerson() {
		return person;
	}

	@Override
	public void setPerson(String name, String address, int age) {
		// TODO Auto-generated method stub
		person.setName(name);
		person.setAddress(address);
		person.setAge(age);
	}

}
