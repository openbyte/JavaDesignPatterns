package com.hero.org;

import java.util.ArrayList;

public class Prototype implements Cloneable {
	private String name;
	private String address;
	private String Phone;
	ArrayList<Model> models = new ArrayList<Model>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public Prototype clone() {
		Prototype prototype = null;
		try {
			prototype = (Prototype) super.clone();
			prototype.models = (ArrayList<Model>) this.models.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return prototype;
	}

	@Override
	public String toString() {
		return "Prototype [name=" + name + ", address=" + address + ", Phone=" + Phone;
	}

}
