package com.hero.org;

public class ConcreteStateB extends State {
	@Override
	public void Handle(Context context) {
		System.out.println(" 这是状态 StateB");
		context.setState(new ConcreteStateC());
	}
}
