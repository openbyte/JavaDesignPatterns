package com.hero.org;

public abstract class State {
	public abstract void Handle(Context context);
}
