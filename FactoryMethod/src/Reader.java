public class Reader {
	public IReader CreateWindow(String type) {
		if (type.equals("Big")) {
			return new TextBok();
		} else {
			return new PDFBook();
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Reader myFactory = new Reader();
		IReader myBigWindow = myFactory.CreateWindow("Big");
		System.out.println(myBigWindow.bookType());
		IReader mySmallWindow = myFactory.CreateWindow("Small");
		System.out.println(mySmallWindow.bookType());
	}
}
