package com.StarMicronics.StarIOSDK;

import java.util.ArrayList;
import java.util.List;

import com.starmicronics.stario.PortInfo;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;

import android.app.*;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class StarIOSDKPOSPrinterRasterModeActivity extends Activity {

	private String strPrintArea = "";
	private Context me = this;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		SharedPreferences pref = getSharedPreferences("pref", MODE_WORLD_READABLE | MODE_WORLD_WRITEABLE);
		portNameField.setText(pref.getString("portName", "TCP:192.168.192.45"));

		InitializeComponent();
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public void Test(View view) {
		Intent myIntent = new Intent(this, helpActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void Help(View view) {
		Intent myIntent = new Intent(this, helpActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void OpenCashDrawer(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		String portName = portNameField.getText().toString();
		String portSettings = getPortSettingsOption(portName);

		PrinterFunctions.OpenCashDrawer(this, portName, portSettings);
	}

	public void GetStatus(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		String portName = portNameField.getText().toString();
		String portSettings = getPortSettingsOption(portName);

		// The portable printer and non portable printer have the same
		PrinterFunctions.CheckStatus(this, portName, portSettings);
	}

	public void ShowBarcode(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, barcodeselector.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowBarcode2d(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, barcodeselector2d.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowCut(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, cutActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowTextFormating(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, textFormatingActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowKanjiTextFormating(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, kanjiTextFormatingActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowRasterPrinting(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, rasterPrintingActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowImagePrinting(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());

		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, imagePrintingActivity.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void ShowQrCodeImagePrinting(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());

		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		Intent myIntent = new Intent(this, PrintQrCodeImage.class);
		startActivityFromChild(this, myIntent, 0);
	}

	public void MCR(View view) {
		EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
		PrinterTypeActivity.setPortName(portNameField.getText().toString());
		PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

		PrinterFunctions.MCRStart(this);
	}

	public void SampleReceipt(View view) {
		final String item_list[] = new String[] { getResources().getString(R.string.printArea3inch), getResources().getString(R.string.printArea4inch), };

		strPrintArea = getResources().getString(R.string.printArea3inch);

		Builder printableAreaDialog = new AlertDialog.Builder(this);
		printableAreaDialog.setIcon(android.R.drawable.checkbox_on_background);
		printableAreaDialog.setTitle("Printable Area List");
		printableAreaDialog.setSingleChoiceItems(item_list, 0, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				strPrintArea = item_list[whichButton];
			}
		});
		printableAreaDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
				PrinterTypeActivity.setPortName(portNameField.getText().toString());
				PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));
				String commandType = "Raster";

				PrinterFunctions.PrintSampleReceipt(me, PrinterTypeActivity.getPortName(), PrinterTypeActivity.getPortSettings(), commandType, getResources(), strPrintArea);
			}
		});
		printableAreaDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//
			}
		});
		printableAreaDialog.show();
	}

	public void SampleRecieptJp(View view) {
		final String item_list[] = new String[] { getResources().getString(R.string.printArea3inch), getResources().getString(R.string.printArea4inch), };

		strPrintArea = getResources().getString(R.string.printArea3inch);

		Builder printableAreaDialog = new AlertDialog.Builder(this);
		printableAreaDialog.setIcon(android.R.drawable.checkbox_on_background);
		printableAreaDialog.setTitle("Printable Area List");
		printableAreaDialog.setSingleChoiceItems(item_list, 0, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				strPrintArea = item_list[whichButton];
			}
		});
		printableAreaDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
				PrinterTypeActivity.setPortName(portNameField.getText().toString());
				PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));
				String commandType = "Raster";

				PrinterFunctions.PrintSampleReceiptJp(me, PrinterTypeActivity.getPortName(), PrinterTypeActivity.getPortSettings(), commandType, strPrintArea);
			}
		});
		printableAreaDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//
			}
		});
		printableAreaDialog.show();
	}

	public void beginEndCheckedBlock(View view) {
		final String item_list[] = new String[] { getResources().getString(R.string.printArea3inch), getResources().getString(R.string.printArea4inch), };

		strPrintArea = getResources().getString(R.string.printArea3inch);

		Builder printableAreaDialog = new AlertDialog.Builder(this);
		printableAreaDialog.setIcon(android.R.drawable.checkbox_on_background);
		printableAreaDialog.setTitle("Printable Area List");
		printableAreaDialog.setSingleChoiceItems(item_list, 0, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				strPrintArea = item_list[whichButton];
			}
		});

		printableAreaDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				int selectedPaperWidthInch = 0;
				if (strPrintArea == getResources().getString(R.string.printArea3inch)) {
					selectedPaperWidthInch = 3;
				} else if (strPrintArea == getResources().getString(R.string.printArea4inch)) {
					selectedPaperWidthInch = 4;
				}

				EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
				PrinterTypeActivity.setPortName(portNameField.getText().toString());
				PrinterTypeActivity.setPortSettings(getPortSettingsOption(PrinterTypeActivity.getPortName()));

				PrinterFunctions.beginEndCheckedBlock(me, PrinterTypeActivity.getPortName(), PrinterTypeActivity.getPortSettings(), selectedPaperWidthInch, "Raster");
			}
		});
		printableAreaDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				//
			}
		});
		printableAreaDialog.show();
	}

	public void PortDiscovery(View view) {
		List<PortInfo> BTPortList;
		List<PortInfo> TCPPortList;
		final EditText editPortName;

		final ArrayList<PortInfo> arrayDiscovery;
		ArrayList<String> arrayPortName;

		arrayDiscovery = new ArrayList<PortInfo>();
		arrayPortName = new ArrayList<String>();

		try {

			BTPortList = StarIOPort.searchPrinter("BT:");
			TCPPortList = StarIOPort.searchPrinter("TCP:");

			for (PortInfo portInfo : BTPortList) {
				arrayDiscovery.add(portInfo);
			}

			for (PortInfo portInfo : TCPPortList) {
				arrayDiscovery.add(portInfo);
			}

			arrayPortName = new ArrayList<String>();

			for (PortInfo discovery : arrayDiscovery) {
				String portName;

				portName = discovery.getPortName();

				if (discovery.getMacAddress().equals("") == false) {
					portName += "\n - " + discovery.getMacAddress();
					if (discovery.getModelName().equals("") == false) {
						portName += "\n - " + discovery.getModelName();
					}
				}

				arrayPortName.add(portName);
			}

		} catch (StarIOPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		editPortName = new EditText(this);

		new AlertDialog.Builder(this).setIcon(R.drawable.icon).setTitle("Please Select IP Address or Input Port Name").setView(editPortName).setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int button) {
				EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
				portNameField.setText(editPortName.getText());

				SharedPreferences pref = getSharedPreferences("pref", MODE_WORLD_READABLE | MODE_WORLD_WRITEABLE);
				Editor editor = pref.edit();
				editor.putString("portName", portNameField.getText().toString());
				editor.commit();
			}
		}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int button) {
			}
		}).setItems(arrayPortName.toArray(new String[0]), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int select) {
				EditText portNameField = (EditText) findViewById(R.id.editText_PortName);
				portNameField.setText(arrayDiscovery.get(select).getPortName());

				SharedPreferences pref = getSharedPreferences("pref", MODE_WORLD_READABLE | MODE_WORLD_WRITEABLE);
				Editor editor = pref.edit();
				editor.putString("portName", portNameField.getText().toString());
				editor.commit();
			}
		}).show();
	}

	private void InitializeComponent() { // delete view of some function button
											// for POS Printer Raster mode
		TextView titleText = (TextView) findViewById(R.id.textView1);
		titleText.setText("Star Micronics POS Printer Samples");

		Spinner spinner_tcp_port_number = (Spinner) findViewById(R.id.spinner_tcp_port_number);
		SpinnerAdapter ad_tcp_port_number = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[] { "Standard", "9100", "9101", "9102", "9103", "9104", "9105", "9106", "9107", "9108", "9109" });
		spinner_tcp_port_number.setAdapter(ad_tcp_port_number);

		Spinner spinner_bluetooth_communication_type = (Spinner) findViewById(R.id.spinner_bluetooth_communication_type);
		SpinnerAdapter ad_bluetooth_communication_type = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, new String[] { "SSP", "PIN Code" });
		spinner_bluetooth_communication_type.setAdapter(ad_bluetooth_communication_type);

		Button MSRButton = (Button) findViewById(R.id.button_MSR);
		MSRButton.setVisibility(View.GONE);

		Button barcode1DButton = (Button) findViewById(R.id.button_Barcodes1D);
		barcode1DButton.setVisibility(View.GONE);

		Button barcode2DButton = (Button) findViewById(R.id.button_BarCodes2D);
		barcode2DButton.setVisibility(View.GONE);

		Button cutPatternButton = (Button) findViewById(R.id.button_Cut);
		cutPatternButton.setVisibility(View.GONE);

		Button textFormattingButton = (Button) findViewById(R.id.button_TextFormating);
		textFormattingButton.setVisibility(View.GONE);

		Button kanjiTextFormattingButton = (Button) findViewById(R.id.button_KanjiTextFormating);
		kanjiTextFormattingButton.setVisibility(View.GONE);
	}

	private String getTCPPortSettings() {
		String portSettings = "";

		Spinner spinner_tcp_port_number = (Spinner) findViewById(R.id.spinner_tcp_port_number);
		switch (spinner_tcp_port_number.getSelectedItemPosition()) {
		case 0:
			portSettings = "";
			break;
		case 1:
			portSettings = "9100";
			break;
		case 2:
			portSettings = "9101";
			break;
		case 3:
			portSettings = "9102";
			break;
		case 4:
			portSettings = "9103";
			break;
		case 5:
			portSettings = "9104";
			break;
		case 6:
			portSettings = "9105";
			break;
		case 7:
			portSettings = "9106";
			break;
		case 8:
			portSettings = "9107";
			break;
		case 9:
			portSettings = "9108";
			break;
		case 10:
			portSettings = "9109";
			break;
		}

		return portSettings;
	}

	private String getBluetoothCommunicationType() {
		String portSettings = "";

		Spinner spinner_bluetooth_communication_type = (Spinner) findViewById(R.id.spinner_bluetooth_communication_type);
		switch (spinner_bluetooth_communication_type.getSelectedItemPosition()) {
		case 0:
			portSettings = "";
			break;
		case 1:
			portSettings = ";p";
			break;
		}

		return portSettings;
	}

	private String getPortSettingsOption(String portName) {
		String portSettings = "";

		if (portName.toUpperCase().startsWith("TCP:")) {
			portSettings += getTCPPortSettings();
		} else if (portName.toUpperCase().startsWith("BT:")) {
			portSettings += getBluetoothCommunicationType(); // Bluetooth option
																// of
																// "portSettings"
																// must be last.
		}

		return portSettings;
	}
}